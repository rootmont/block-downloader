import jsonpickle
import logging
from pathlib import Path
from typing import Union

from config import *
from model.block_info import BlockInfo
from repository.remote_repository import RemoteRepository


class LocalRepository(RemoteRepository):
    def get_block_info(self, block_num: int) -> Union[None, BlockInfo]:
        try:
            block_json = Path(self._get_file_path(block_num)).read_text()
            return jsonpickle.loads(block_json)
        except Exception as e:
            logging.warning(e)
            return None

    def save_block_info(self, block_num: int, block_info: BlockInfo):
        json_dumped = jsonpickle.dumps(block_info)
        with open(self._get_file_path(block_num), 'w') as file:
            file.write(json_dumped)

    def _get_file_path(self, block_num: int):
        return LOCAL_STORAGE_PATH + str(block_num) + '.json'
