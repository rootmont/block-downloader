from typing import Union

from model.block_info import BlockInfo


class RemoteRepository(object):
    def get_block_info(self, block_num: int) -> Union[None, BlockInfo]:
        raise NotImplementedError

    def save_block_info(self, block_num: int, block_info: BlockInfo):
        raise NotImplementedError
