from config import *
from repository.local_repository import LocalRepository
from repository.remote_repository import RemoteRepository
from repository.s3_repository import S3Repository


def get_storage() -> RemoteRepository:
    if RUNNING_ENV != "prod":
        return LocalRepository()
    else:
        return S3Repository()
