import boto3
import jsonpickle
import logging
from typing import Union

from config import *
from model.block_info import BlockInfo
from repository.remote_repository import RemoteRepository


class S3Repository(RemoteRepository):
    __s3 = boto3.resource('s3', region_name=AMAZON_REGION, aws_access_key_id=AMAZON_ID, aws_secret_access_key=AMAZON_KEY)

    def get_block_info(self, block_num: int) -> Union[None, BlockInfo]:
        try:
            obj = self.__s3.Object(BLOCK_BUCKET, self._get_file_name(block_num))
            loaded = obj.get()['Body'].read().decode('utf-8')
            return jsonpickle.loads(loaded)
        except Exception as e:
            logging.warning(e)
            return None

    def save_block_info(self, block_num: int, block_info: BlockInfo):
        self.__s3.Object(BLOCK_BUCKET, self._get_file_name(block_num)).put(Body=jsonpickle.dumps(block_info))

    def _get_file_name(self, block_num: int):
        return str(block_num) + '.json'
