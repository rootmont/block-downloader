import logging
import os

from app import application
from tasks.ethereum_block_listener import BlockListener


class NoRunningFilter(logging.Filter):
    def filter(self, record):
        return "maximum number of running" not in record.msg


logging.basicConfig(level=logging.INFO)
appschedule_filter = NoRunningFilter()
logging.getLogger("apscheduler.scheduler").addFilter(appschedule_filter)

if __name__ == '__main__':
    os.environ.get('RUNNING_ENV', 'local')
    application.scheduler.add_job(lambda: BlockListener().start(),
                                  trigger='interval',
                                  seconds=3,
                                  max_instances=4)
    application.run()
