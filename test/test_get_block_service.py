import unittest

from service.get_block_service import GetBlockService


class TestStringMethods(unittest.TestCase):
    get_block_service = None

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.get_block_service = GetBlockService()

    def test_get_block_remote(self):
        block_info = self.get_block_service.get_block_remote(6000000)
        assert block_info is not None, "Should return a value"
        assert len(block_info.logs) > 0, "Should have logs"

