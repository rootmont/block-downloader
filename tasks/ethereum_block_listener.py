import pika
import json
import logging

from config import *
from repository.storage_factory import get_storage
from service.get_block_service import GetBlockService


class BlockListener(object):
    def __init__(self):
        self._credentials = pika.PlainCredentials(RABBITMQ_USER, RABBITMQ_PASS)
        self._connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=RABBITMQ_HOST, credentials=self._credentials)
        )
        self._channel = self._connection.channel()
        # declare queue
        self._channel.queue_declare(queue=ETHEREUM_FETCH_BLOCK_QUEUE, durable=True)
        self._channel.basic_qos(prefetch_count=10)
        self._channel.basic_consume(self.callback, queue=ETHEREUM_FETCH_BLOCK_QUEUE)

        self._ethereum_block_service = GetBlockService()
        self._repository = get_storage()

    def callback(self, ch, method, properties, body):
        decoded_work = json.loads(body)
        block = self._ethereum_block_service.get_block_remote(decoded_work['block_num'])
        self._repository.save_block_info(decoded_work['block_num'], block)
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def start(self):
        logging.info("Started listening for incoming blocks to fetch")
        self._channel.start_consuming()
