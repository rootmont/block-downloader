from typing import List
import json
import decimal

"""
These classes have to remain in BlockInfo due to jsonpickle issues.
If the package name is not the same for jsonpickle, it cannot determine the right python class.
"""


class LogsInfo(object):
    def __init__(self, address: str, topics: List[str], data: str):
        self.address = address
        self.topics: List[str] = topics
        self.data = data


class BlockInfo(object):
    def __init__(self,
                 block_num: int,
                 num_transactions: int,
                 logs: List[LogsInfo],
                 time_stamp: int,
                 transactions_value: decimal.Decimal,
                 block_reward_value: decimal.Decimal,
                 uncles_rewards_value: decimal.Decimal):
        self.block_num = block_num
        self.num_transactions = num_transactions
        self.logs = logs
        self.time_stamp = time_stamp
        self.transactions_value = transactions_value
        self.block_reward_value = block_reward_value
        self.uncles_rewards_value = uncles_rewards_value
        self.total_value = transactions_value + block_reward_value + uncles_rewards_value


class BlocksInfoEncoder(json.JSONEncoder):
    def default(self, o):
        return {'__{}__'.format(o.__class__.__name__): o.__dict__}
