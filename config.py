import os

RUNNING_ENV = os.environ.get('RUNNING_ENV', 'local')

RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST', 'rabbitmq-mgmt')
RABBITMQ_USER = os.environ.get('RABBITMQ_USER', 'guest')
RABBITMQ_PASS = os.environ.get('RABBITMQ_PASS', 'guest')

LOCAL_STORAGE_PATH = "./blocks/"

AMAZON_ID = os.environ.get('AMAZON_ID', '')
AMAZON_KEY = os.environ.get('AMAZON_KEY', '')
AMAZON_REGION = "us-west-1"
BLOCK_BUCKET = "eth-block-store"

ETHEREUM_FETCH_BLOCK_QUEUE = "ethereum_blocks"