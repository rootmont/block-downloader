from flask import Flask, Response
from apscheduler.schedulers.background import BackgroundScheduler

from config import *


class Application:
    def __init__(self):
        self._scheduler = BackgroundScheduler()

        self._app = Flask(__name__)
        # self._app.debug = bool(self._config['debug'])
        if RUNNING_ENV == "local":
            self._app.debug = True
        else:
            self._app.debug = False

    def run(self):
        self._scheduler.start()
        self._app.run(host="0.0.0.0", port=5000)

    @property
    def flask_app(self):
        return self._app

    @property
    def scheduler(self):
        return self._scheduler


application = Application()


@application.flask_app.route('/health',  methods=['GET'])
def health():
    return Response("{}", status=200, mimetype='application/json')
