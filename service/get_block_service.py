import logging
import decimal
import functools
import operator
import concurrent
from concurrent.futures.thread import ThreadPoolExecutor

from web3 import Web3, HTTPProvider
from typing import Union, List

from http_provider_custom import HTTPProviderCustom
from model.block_info import BlockInfo, LogsInfo


class GetBlockService(object):
    def __init__(self):
        self._provider = HTTPProviderCustom('https://mainnet.infura.io/v3/73827c14a02c45a79424714be7fab8eb')
        self._web3 = Web3(self._provider)

    def get_chain_height_remote(self) -> int:
        return self._web3.eth.blockNumber

    def _get_uncle_reward(self, miner_reward, block_number, uncle_index) -> decimal.Decimal:
        uncle = self._web3.eth.getUncleByBlock(block_number, uncle_index)
        return decimal.Decimal(int(uncle['number'], 16) + 8 - block_number) * decimal.Decimal(miner_reward / 8)

    def _get_transaction_info(self, transacion_hash):
        # Need this so we don't max out the connections
        fetched_transaction = self._web3.eth.getTransaction(transacion_hash.hex())
        value = fetched_transaction['value']
        transacion_value = Web3.fromWei(value, 'ether')
        receipt = self._web3.eth.getTransactionReceipt(transacion_hash.hex())
        # Update for transaction gas
        block_reward_update = Web3.fromWei(receipt['gasUsed'] * fetched_transaction['gasPrice'], 'ether')
        logs = [LogsInfo(log['address'], [t.hex() for t in log['topics']], log['data']) for log in receipt['logs']]
        return transacion_value, block_reward_update, logs

    def get_block_remote(self, block_num: int) -> Union[BlockInfo, None]:
        logging.info("Fetching information for block: {}".format(block_num))
        block = self._web3.eth.getBlock(block_num)

        if block is None:
            return None

        logs: List[LogsInfo] = []
        block_num = block['number']

        miner_reward = decimal.Decimal(0.0)
        if block_num >= 4370000:
            miner_reward = decimal.Decimal(3.0)
        else:
            miner_reward = decimal.Decimal(5.0)

        transactions_value = decimal.Decimal(0.0)

        # Inclusion reward
        block_reward = miner_reward
        block_reward += (decimal.Decimal(len(block['uncles'])) * miner_reward) / decimal.Decimal(32)

        total_logs: List[LogsInfo] = []

        # transacion_value, block_reward_update, logs
        with ThreadPoolExecutor(max_workers=6) as executor:
            futures = {executor.submit(self._get_transaction_info, transaction) for transaction in block['transactions']}
            for future in concurrent.futures.as_completed(futures):
                transaction_value, block_reward_update, logs = future.result()
                transactions_value += transaction_value
                block_reward += block_reward_update
                total_logs.extend(logs)

        uncles_rewards = functools.reduce(
            operator.add,
            [self._get_uncle_reward(miner_reward, block_num, index) for index in range(0, len(block['uncles']))],
            decimal.Decimal(0.0)
        )

        return BlockInfo(
            block_num=block_num,
            num_transactions=len(block['transactions']),
            logs=total_logs,
            time_stamp=block['timestamp'],
            transactions_value=transactions_value,
            block_reward_value=block_reward,
            uncles_rewards_value=uncles_rewards
        )
